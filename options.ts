
export interface IOptions {
    verbose: boolean;
}

export const defaultOptions: IOptions = {
    verbose: false,
};
