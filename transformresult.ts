
export class TransformResult {
    public filename: string;
    public contents: string;
}
