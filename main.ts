
import * as commander from "commander";

import * as options from "./options";
import { IOptions } from "./options";
import * as transform from "./transform";

import * as packageInfo from "./package.json";

commander
    .version(packageInfo.version)
    .option("-v, --verbose", "Verbose output", false)
    .arguments("[files...]")
    .action((files: string[], cmdOpts: IOptions) => {

        // We copy the options, because the thing
        // passed in from 'commander' has other
        // junk on it.
        const opts: IOptions = {
            verbose: cmdOpts.verbose,
        };

        // Go!
        processFiles(files, opts);
    })
    .parse(process.argv);

function processFiles(files: string[], opts: IOptions): void {

    const engine = new transform.Transformer();
    engine.opts = opts;
    engine.files = files;

    for (const result of engine.transform()) {
        console.log(result.contents);
    }
}
