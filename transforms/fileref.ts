
import * as ts from "typescript";

// A transformer factory returns a transformer given as transformer-context.
export const transformerFactory: ts.TransformerFactory<ts.SourceFile> = (context) => {

    // A transformer takes a node and returns it. It is *not* iteravely called
    // throughout the tree - if you need that you need to do it yourself.
    // It might be wise to combine multipe passes in to one to avoid multiple loops?
    const transformer: ts.Transformer<ts.SourceFile> = (file) => {

        // A 'visitor' is a function (node -> node) which may replace
        // or null-out the passed-in node. It is useful with helper
        // functions like ts.visit (to perform a replacement on a single
        // node) or ts.visitChildren (to perform replacements on many
        // nodes).
        //
        // Here we check to see if we are called on the node we want to
        // manipulate, and if not the visitor itself decides to recurse
        // and perform replacements further down the tree.
        const visitor: ts.Visitor = (node) => {
            switch (node.kind) {
                case ts.SyntaxKind.Identifier: {
                    const identNode = node as ts.Identifier;
                    if (identNode.text === "___filename") {
                        // TODO - interact with the type-checker to make sure this is the global,
                        // and not a shadowed reference.
                        // We need the root program passed in to make that work, though.
                        return ts.createLiteral("lol_some_file.ts");
                    } else {
                        return node;
                    }
                }
                default: {
                    return ts.visitEachChild(node, visitor, context);
                }
            }
        };

        // This is our actual transformer - we execute
        // our visitor on the top-level node (the SourceFile).
        return ts.visitNode(file, visitor);
    };

    return transformer;
};
