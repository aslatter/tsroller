import * as fs from "fs";

import * as options from "./options";
import { IOptions } from "./options";
import { TransformResult } from "./transformresult";
import * as fileref from "./transforms/fileref";

import * as ts from "typescript";

export class Transformer {
    public opts: IOptions = options.defaultOptions;
    public files: string[] = [];

    public transform(): TransformResult[] {
        const result: TransformResult[] = [];
        const printer = ts.createPrinter();
        for (const fileName of this.files) {

            const sourceFile = ts.createSourceFile(fileName, fs.readFileSync(fileName, "utf8"), ts.ScriptTarget.ES2015);

            // This is where we actually do our transformations!
            const transformedFile = ts.transform(sourceFile, [fileref.transformerFactory]);

            const transformResult = new TransformResult();
            transformResult.filename = sourceFile.fileName;
            transformResult.contents = printer.printFile(transformedFile.transformed[0]);
            result.push(transformResult);
        }

        return result;
    }
}
